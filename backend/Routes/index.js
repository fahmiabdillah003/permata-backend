import express from "express";
import {
  getUsers,
  Register,
  Login,
  Logout,
} from "../Controller/UserController.js";
import { getNotif, sendNotif } from "../Controller/NotifController.js";
import { verifyToken } from "../Middleware/verifyToken.js";
import { refreshToken } from "../Controller/RefreshToken.js";

const router = express.Router();

router.get("/users", verifyToken, getUsers);
router.post("/register", Register);
router.post("/login", Login);
router.get("/token", refreshToken);
router.get("/notif", getNotif);
router.post("/sendnotif", sendNotif);
router.delete("/logout", Logout);

export default router;
