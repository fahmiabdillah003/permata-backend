import Notification from "../Models/NotifModels.js";

export const getNotif = async (req, res) => {
  try {
    const Notif = await Notification.findAll({
      attributes: [
        "id",
        "kode_perangkat",
        "waktu",
        "cabang",
        "statuss",
        "kejadian",
        "lastonline",
      ],
    });
    res.json(Notif);
  } catch (error) {
    console.log(error);
  }
};

export const sendNotif = async (req, res) => {
  let { kode_perangkat, cabang, statuss, kejadian, lastonline } = req.body;
  let date = new Date().getTime();
  console.log(kode_perangkat, cabang, statuss, kejadian, lastonline);
  try {
    let cekPerangkat = await Notification.findAll({
      where: {
        kode_perangkat: kode_perangkat,
      },
    });
    // console.log(cekPerangkat);
    if (cekPerangkat.length > 0) {
      await Notification.update(
        {
          statuss: statuss,
        },
        {
          where: {
            kode_perangkat: cekPerangkat[0].kode_perangkat,
          },
        }
      );
    } else {
      await Notification.create({
        kode_perangkat: kode_perangkat,
        waktu: date,
        cabang: cabang,
        statuss: statuss,
        kejadian: kejadian,
        lastonline: lastonline,
      });
    }

    res.json({
      data: cekPerangkat.length,
      msg: "Mengirim Notifikasi Berhasil",
    });
  } catch (error) {
    console.log(error);
  }
};
