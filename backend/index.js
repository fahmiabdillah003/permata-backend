import express from "express";
import db from "./config/Database.js";
import dotenv from "dotenv";
// import Users from "./Models/UserModels.js";
import cookieParser from "cookie-parser";
import router from "./Routes/index.js";
import cors from "cors";

dotenv.config();

const app = express();
try {
  await db.authenticate();
  console.log("Database Connected ....!");

  // membuat table
  // await Users.sync();
} catch (error) {
  console.error(error);
}
app.use(cors({ credentials: true, origin: "http://localhost:3000" }));
app.use(cookieParser());
app.use(express.json());
app.use(router);

app.listen(5000, () => console.log("Server Berjalan Pada PORT 5000"));
